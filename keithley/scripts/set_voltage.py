#!/usr/bin/env python
from __future__ import print_function, division
#Usage: Provide voltage as <value>V to the script

import sys
from .. import keithley
import time
import numpy as np


def main():
    instr = keithley(verbose=False)

    #Set Voltage
    if len(sys.argv) > 1:
        voltage = sys.argv[1]
        voltage = float(voltage.replace("V", ""))
        print("Setting voltage to {:.3f}V...".format(voltage))
        instr.set_val(voltage)

        timelim = 3 #s
        delay = 0.1 #s
        rc = 1
        for secs in np.arange(0, timelim, delay):
            time.sleep(delay)
            readback = instr.meas("V")
            if abs(readback-voltage) < 0.01: #V
                rc = 0
                time.sleep(delay)
                break


        if rc == 0:
            print("Reached {:.3f}V!".format(readback))
            sys.exit(0) #Success

        else:
            print("Could not reach {:.3f}V (currently: {:.3f}V)".format(voltage, readback))
            sys.exit(1)

    #Read Voltage
    else:
        readback = instr.meas("V")
        print(readback)


if __name__ == "__main__":
    main()
