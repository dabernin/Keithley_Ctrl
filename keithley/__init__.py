import os
import sys
import usbtmc


def list_devices(only_keithleys=True):
    devices = usbtmc.list_devices()
    if only_keithleys:
        devices = [ dev for dev in devices if is_keithley(dev) ]

    return devices

def is_keithley(device):
    return device.iManufacturer == 2

class keithley(object):
    "Keithley USBTMC Interface"
    def __init__(self, vendorID=None, productID=None, verbose=True):
        "Create new keithley instance."
        if vendorID is None and productID is None:
            devices = list_devices(only_keithleys=True)
            if len(devices) == 0:
                raise SystemError("No Keithleys found!")
            elif len(devices) > 1:
                raise SystemError("Multiple Keithleys found! Please specify vendor and product ID!")
            else:
                self.idVendor = devices[0].idVendor
                self.idProduct = devices[0].idProduct
        elif vendorID and productID:
            self.idVendor = vendorID
            self.idProduct = productID
        else:
            raise ValueError("Both vendor and product ID have to be specified if any is provided!")

        self.verbose = verbose

        self.instr = usbtmc.Instrument(self.idVendor, self.idProduct)
        self.connected = self.instr.connected

        self.set_mode("V")  #Voltage mode

    def __del__(self):
        self.close()

    def close(self):
        self.instr.close()

    def set_verbosity(self, on):
        self.verbose = on

    def send_tsp(self, command):
        "Send TSP command to Keithley"
        self.instr.write(command)

    def read_tsp(self, command):
        "Send TSP command and read value from Keithley"
        return self.instr.ask("print({})".format(command))

    def set_power(self, on):
        "Power on/off the output"
        self.send_tsp("smua.source.output = {}".format("1" if on else "0"))

    def get_power(self):
        "Print on/off the output"
        return int(float(self.read_tsp("smua.source.output")))

    def set_mode(self, mode):
        "Set mode to V (Voltage) or A (Current)"
        if mode == "V":
            self.send_tsp("smua.source.func = 1")
            if self.verbose: print("Voltage mode enabled")
            self.mode = "V"
        elif mode == "A":
            self.send_tsp("smua.source.func = 0")
            if self.verbose: print("Current mode enabled")
            self.mode = "A"
        else:
            raise ValueError("Mode must be either V or A")

    def set_lim(self, value):
        "Set limit depending on currently selected mode"
        if self.mode == "V":
            self.send_tsp("smua.source.limiti = {}".format(value))
            if self.verbose: print("Current limit set to {}A.".format(value))
        elif self.mode == "A":
            self.send_tsp("smua.source.limitv = {}".format(value))
            if self.verbose: print("Voltage limit set to {}V.".format(value))

    def get_lim(self):
        "Get limit depending on currently selected mode"
        if self.mode == "V":
            if self.verbose: print("Getting limit on current...")
            suffix = "i"
        else:
            if self.verbose: print("Getting limit on voltage...")
            suffix = "v"

        return float(self.read_tsp("smua.source.limit{}".format(suffix)))


    def get_val(self):
        "Get val depending on currently selected mode"
        if self.mode == "V":
            if self.verbose: print("Getting level of voltage...")
            suffix = "v"
        else:
            if self.verbose: print("Getting level of current...")
            suffix = "i"

        return float(self.read_tsp("smua.source.level{}".format(suffix)))


    def set_val(self, value):
        "Set value depending on currently selected mode"
        if self.mode == "V":
            self.send_tsp("smua.source.levelv = {}".format(value))
            if self.verbose: print("Voltage set to {}V.".format(value))
        if self.mode == "A":
            self.send_tsp("smua.source.leveli = {}".format(value))
            if self.verbose: print("Current set to {}i.".format(value))

    def meas(self, mode):
        "Perform measurement of V(volts), A(mperes), R(resistance), P(ower)"
        if mode == "V":
            suffix = "v"
            if self.verbose: print("Measuring voltage...")
        elif mode == "A":
            suffix = "i"
            if self.verbose: print("Measuring current...")
        elif mode == "R":
            suffix = "r"
            if self.verbose: print("Measuring resistance...")
        elif mode == "P":
            suffix = "p"
            if self.verbose: print("Measuring power...")
        else:
            raise ValueError("Mode must be V,A,R or P!")

        return float(self.read_tsp("smua.measure.{}()".format(suffix)))

