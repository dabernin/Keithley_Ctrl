from setuptools import setup

setup(
    name='keithley',
    version='1.0',
    author='Daniel Berninghoff',
    author_email='daniel.berninghoff@gmail.com',
    packages=["keithley",
              "keithley.scripts"],
    install_requires=[
        'python-usbtmc>=0.8',
        'pyusb',
        'numpy'
    ],
    entry_points={
        'console_scripts': [
            'keithley_set_voltage = keithley.scripts.set_voltage:main',
        ]
    }

)

